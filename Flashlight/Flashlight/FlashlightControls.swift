//
//  FlashlightControls.swift
//  Flashlight
//
//  Created by Night Dog on 10/14/18.
//  Copyright © 2018 Night Dog. All rights reserved.
//

import UIKit
class FlashlightControls : UIViewController{
    
    @IBOutlet weak var lblRemind: UILabel!
    @IBOutlet weak var colorDisplayView: UIView!
    var bgColor = UIColor.white;
    override func viewDidLoad() {
        super .viewDidLoad()
        colorDisplayView.backgroundColor = bgColor;
        let tap = UITapGestureRecognizer(target: self, action: #selector(doubleTapped))
        tap.numberOfTapsRequired = 2
        self.colorDisplayView.addGestureRecognizer(tap);
//       let result = DB.Get(key: DB.Key.AlreadyTapped)
//        if result == "TRUE"{
//            lblRemind.text = "";
//        }
    }
    @objc func doubleTapped (){
        let des = storyboard?.instantiateViewController(withIdentifier: "SettingsID") as! MainController;
        self.present(des,animated: true,completion: nil);
//        DB.Set(key: DB.Key.AlreadyTapped, value: "TRUE");
    }
    
}
