//
//  helper.swift
//  Flashlight
//
//  Created by Night Dog on 10/13/18.
//  Copyright © 2018 Night Dog. All rights reserved.
//

import Foundation
import UIKit
class UI{
    static func addDoneButton (controls : [UITextField]){
        for textField in controls {
            let toolbar = UIToolbar();
            toolbar.items = [UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil),UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target:textField, action: #selector(UITextField.resignFirstResponder))];
            toolbar.sizeToFit();
            textField.inputAccessoryView = toolbar;
        }
        
    }
}
class DB{
    class Key{
        static var AlreadyTapped = "ALREADY"
    }
    static func Set(key : String, value: String)
    {
        UserDefaults.standard.set(value,forKey : key);
        
    }
    static func Get(key : String)->String
    {
        return UserDefaults.standard.string(forKey: key)!;
        
    }
    
}
