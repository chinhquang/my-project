//
//  ViewController.swift
//  Flashlight
//
//  Created by Night Dog on 10/10/18.
//  Copyright © 2018 Night Dog. All rights reserved.
//

import UIKit
import Foundation
class MainController: UIViewController {

    
    @IBOutlet weak var viewDisplay: UIView!
    @IBOutlet weak var txtRed: UITextField!
    @IBOutlet weak var txtGreen: UITextField!
    @IBOutlet weak var txtBlue: UITextField!
    @IBOutlet weak var txtAlpha: UITextField!
    @IBOutlet weak var slider_Red: UISlider!
    @IBOutlet weak var slider_Green: UISlider!
    @IBOutlet weak var slider_Blue: UISlider!
   
    @IBOutlet weak var scrollview: UIScrollView!
    
    @IBOutlet weak var slider_Alpha: UISlider!
    
    @IBOutlet weak var btnOK: UIButton!
    @IBAction func sliderRed_Action(_ sender: UISlider) {
        txtRed.text = String(slider_Red.value);
        viewDisplay.backgroundColor = GetColor();
    }
    @IBAction func
        sliderGreen_Action(_ sender: UISlider) {
        txtGreen.text = String(slider_Green.value);
       viewDisplay.backgroundColor = GetColor();
    }
    @IBAction func sliderBlue_Action(_ sender: UISlider) {
        txtBlue.text = String(slider_Blue.value);
        viewDisplay.backgroundColor = GetColor();
    }
    @IBAction func sliderAlpha_Action(_ sender: UISlider) {
        txtAlpha.text = String(slider_Alpha.value);
        
        viewDisplay.backgroundColor = GetColor();
    }
    
    @IBAction func txtRed_ValueChange(_ sender: UITextField) {
        if(txtRed.text!.count > 0){
            slider_Red.value = Float(Int(txtRed.text!)!);
            viewDisplay.backgroundColor = GetColor();
        }
        
    }
    @IBAction func txtGreen_ValueChanged(_ sender: UITextField) {
        if(txtGreen.text!.count > 0){
            slider_Green.value = Float(Int(txtGreen.text!)!);
            viewDisplay.backgroundColor = GetColor();
        }
    }
    @IBAction func txtBlue_ValueChange(_ sender: UITextField) {
        if(txtBlue.text!.count > 0){
            slider_Blue.value = Float(Int(txtBlue.text!)!);
            viewDisplay.backgroundColor = GetColor();
        }
    }
    @IBAction func txtAlpha_ValueChange(_ sender: UITextField) {
        if(txtAlpha.text!.count > 0){
            slider_Alpha.value = Float(Int(txtAlpha.text!)!);
            viewDisplay.backgroundColor = GetColor();
        }
    }
    func GetColor ()->UIColor{
        let red = CGFloat(slider_Red.value)/255;
        let blue = CGFloat(slider_Blue.value)/255;
        let green = CGFloat(slider_Green.value)/255;
        let alpha = CGFloat(slider_Alpha.value)/255;
        
    let color = UIColor(red:red,green:green,blue:blue,alpha:alpha);
        return color
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        UI.addDoneButton(controls: [txtBlue,txtRed,txtGreen,txtAlpha])
        registerKeyboardNotification()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super .viewDidDisappear(animated)
         deregidterKeyboardNotification()
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        scrollview.isScrollEnabled = true
        let info = notification.userInfo;
        let keyboard = (info?[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue.size
        let contentInset = UIEdgeInsets(top: 0.0,left: 0.0,bottom: keyboard.height,right: 0.0);
        self.scrollview.contentInset = contentInset;
        self.scrollview.scrollIndicatorInsets = contentInset;
    }
    
    @objc func keyboardWillHide(notification: NSNotification){
        
        let info = notification.userInfo;
        let keyboard = (info?[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue.size
        let contentInset = UIEdgeInsets(top: 0.0,left: 0.0,bottom: -keyboard.height,right: 0.0);
        self.scrollview.contentInset = contentInset;
        self.scrollview.scrollIndicatorInsets = contentInset;
    }
    func registerKeyboardNotification(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    func deregidterKeyboardNotification(){
        NotificationCenter.default.removeObserver(self,name: UIResponder.keyboardWillShowNotification,object : nil)
        NotificationCenter.default.removeObserver(self,name: UIResponder.keyboardWillHideNotification,object : nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowFlashLight"{
            let dest = segue.destination as! FlashlightControls;
            dest.bgColor = GetColor();
            
        }
    }
}

